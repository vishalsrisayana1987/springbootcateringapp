package com.caterer.demo.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.caterer.demo.Model.Caterer;

public interface CatererRepository extends MongoRepository<Caterer, Long>{
	
	
	Caterer findByName(String name);
	
	

}
