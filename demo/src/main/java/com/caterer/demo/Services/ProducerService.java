package com.caterer.demo.Services;

import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.caterer.demo.Model.Caterer;

@Service
public final class ProducerService {
    private static final Logger logger = LoggerFactory.getLogger(ProducerService.class);
    
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String TOPIC = "kafkaTopic";

    public ProducerService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }
    
    JSONObject jsonObj = new JSONObject();
public void sendMessage(Caterer caterer
		//String message
		) {
logger.info(String.format("$$$$ => Producing message: %s", caterer));

ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(TOPIC, caterer.toString());
future.addCallback(new ListenableFutureCallback<>() {
@Override
public void onFailure(Throwable ex) {
logger.info("Unable to send message=[ {} ] due to : {}", caterer.toString(), ex.getMessage());
}

@Override
public void onSuccess(SendResult<String, String> result) {
logger.info("Sent message=[ {} ] with offset=[ {} ]", caterer, result.getRecordMetadata().offset());
}
});
}

public void sendMessage(Optional<Caterer> caterer
		//String message
		) {
logger.info(String.format("$$$$ => Producing message: %s", caterer));

ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(TOPIC, caterer.toString());
future.addCallback(new ListenableFutureCallback<>() {
@Override
public void onFailure(Throwable ex) {
logger.info("Unable to send message=[ {} ] due to : {}", caterer.toString(), ex.getMessage());
}

@Override
public void onSuccess(SendResult<String, String> result) {
logger.info("Sent message=[ {} ] with offset=[ {} ]", caterer, result.getRecordMetadata().offset());
}
});
}

public void sendMessageList(List<Caterer> cl
		//String message
		) {
logger.info(String.format("$$$$ => Producing message: %s", cl));

ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(TOPIC, cl.toString());
future.addCallback(new ListenableFutureCallback<>() {
@Override
public void onFailure(Throwable ex) {
logger.info("Unable to send message=[ {} ] due to : {}", cl.toString(), ex.getMessage());
}

@Override
public void onSuccess(SendResult<String, String> result) {
logger.info("Sent message=[ {} ] with offset=[ {} ]", cl, result.getRecordMetadata().offset());
}
});
}

public void sendMessageListPage(Page<Caterer> cl
		//String message
		) {
logger.info(String.format("$$$$ => Producing message: %s", cl));

ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(TOPIC, cl.toString());
future.addCallback(new ListenableFutureCallback<>() {
@Override
public void onFailure(Throwable ex) {
logger.info("Unable to send message=[ {} ] due to : {}", cl.toString(), ex.getMessage());
}

@Override
public void onSuccess(SendResult<String, String> result) {
logger.info("Sent message=[ {} ] with offset=[ {} ]", cl, result.getRecordMetadata().offset());
}
});
}
}