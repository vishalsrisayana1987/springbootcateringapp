package com.caterer.demo.Services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.caterer.demo.Model.Caterer;

@Service
public final class ConsumerService {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    @KafkaListener(topics = "kafkaTopic", groupId = "group_id")
    public Caterer consume(Caterer c
    		//String message
    		) {
        logger.info(String.format("$$$$ => Consumed message: %s", c));
        
        return  c;
    }
}