package com.caterer.demo.Model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Capacity {

	
	private int minValue;//min 1, cannot be blank
	private int maxVAlue;// must not be les than min, cannot be blank, c
	public int getMinValue() {
		return minValue;
	}
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	public int getMaxVAlue() {
		return maxVAlue;
	}
	public void setMaxVAlue(int maxVAlue) {
		this.maxVAlue = maxVAlue;
	}
	@Override
	public String toString() {
		return "Capacity [minValue=" + minValue + ", maxVAlue=" + maxVAlue + "]";
	}
	
	
}
