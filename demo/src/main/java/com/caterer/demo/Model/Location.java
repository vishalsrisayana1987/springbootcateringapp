package com.caterer.demo.Model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Location {
	
	private String cityName;
	private String StreetName_Number;
	
	//optional
	private String postalCode;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStreetName_Number() {
		return StreetName_Number;
	}

	public void setStreetName_Number(String streetName_Number) {
		StreetName_Number = streetName_Number;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return "Location [cityName=" + cityName + ", StreetName_Number=" + StreetName_Number + ", postalCode="
				+ postalCode + "]";
	}
	
	
	

}
