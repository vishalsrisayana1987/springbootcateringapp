package com.caterer.demo.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "database_sequences")
@Getter @Setter
public class DatabaseSequence {
	
	   @Id
	    private String id;

	    private long seq;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public long getSeq() {
			return seq;
		}

		public void setSeq(long seq) {
			this.seq = seq;
		}
	    
	    


}
