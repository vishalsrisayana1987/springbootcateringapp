package com.caterer.demo.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collation = "caterers")
public class Caterer {

	
	  @Transient
	    public static final String SEQUENCE_NAME = "caterers_sequence";
	@Id
	private String id;
	private String catererName;
	private Location location;
	private Capacity capacity;
	private ContactInfo contactInfo;
	@Override
	public String toString() {
		return "Caterer [id=" + id + ", catererName=" + catererName + ", location=" + location + ", capacity="
				+ capacity + ", contactInfo=" + contactInfo + "]";
	}
	
	
	
	
	
}
