package com.caterer.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.caterer.demo.Model.Caterer;
import com.caterer.demo.Repository.CatererRepository;
import com.caterer.demo.Services.ConsumerService;
import com.caterer.demo.Services.ProducerService;

@RestController
@RequestMapping("/caterers")
public class AppController {

	@Autowired
	private CatererRepository catererRepository;
	
	@Autowired
	 private  ProducerService producerService; 
	
	@Autowired
	 private  ConsumerService consumerService; 
	
	private Caterer caterer;
	

	@PostMapping("/addCaterer")
	public ResponseEntity<?> addCaterer(@RequestBody Caterer caterer) {
		Caterer save = this.catererRepository.save(consumerService.consume(caterer));
		return ResponseEntity.ok(save);
	}
	
	@GetMapping("/getCaterers")
	public List<Caterer> listCaterer() {
		
		producerService.sendMessageList(this.catererRepository.findAll());
		return this.catererRepository.findAll();
	}
	
	@GetMapping("/getCaterersByPage")
	public Page<Caterer> listCatererByPage(Pageable p) {
		producerService.sendMessageListPage(this.catererRepository.findAll(p));
		return this.catererRepository.findAll(p);
	}
	
	
	@GetMapping("/getCatererByID/{id}")
	public ResponseEntity<?> getCatererByID(@PathVariable Long id	) {
		producerService.sendMessage(this.catererRepository.findById(id));
		return ResponseEntity.ok(this.catererRepository.findById(id));
	}
	
	
	@GetMapping("/getCatererByName/{name}")
	public ResponseEntity<?> getCatererByName(@PathVariable String  name	) {
		
		producerService.sendMessage(this.catererRepository.findByName(name));
		return ResponseEntity.ok(this.catererRepository.findByName(name));
	}
	
	

}
